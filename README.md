[![pipeline status](https://gitlab.com/master-cloud-apps/patrones-arquitectura-sw/mastermind/badges/master/pipeline.svg)](https://gitlab.com/master-cloud-apps/patrones-arquitectura-sw/mastermind/-/commits/master)
[![coverage](https://gitlab.com/master-cloud-apps/patrones-arquitectura-sw/mastermind/badges/master/coverage.svg)](https://gitlab.com/master-cloud-apps/patrones-arquitectura-sw/mastermind/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Amastermind&metric=alert_status)](https://sonarcloud.io/dashboard?id=org.eyo%3Amastermind)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Amastermind&metric=coverage)](https://sonarcloud.io/dashboard?id=org.eyo%3Amastermind)

# Mastermind.

## Requisitos Graficos.

Toda la información se encuentra en [wikipedia][1] o en [youtube][2].

* **Funcionalidad**: Básica + undo/redo
* **Interfaz**: Texto
* **Distribución**: Cliente/Servidor y Satandalone
* **Persistencia**: No

## Modelo del dominio

![Domain Model](docs/domain_model.png)

## Diseño de clases final

![Design Model](docs/design_model.png)

## Decisiones de diseño/mejoras

* Se diseñan dos clases nuevas para realizar el cliente/servidor. MastermindClient.java y MastermindServer.java
. Ambas clases disponen de un método `main` para poder lanzarse como línea de comandos.
* En la parte del cliente se incluye una interfaz de comunicaciones con Sockets que se encargará de solicitar al
 servidor toda la información que necesite del juego.
* En la parte del servidor se ha creado un Dispatcher encargado de responder al cliente por cada una de las posibles
 peticiones del juego (si el juego está terminado, el listado de resultados para pintarlos en la pantalla, recuperar
  el valor de la combincación propuesta, etc...).
* Se ha utilizado el patrón Proxy para poder reutilizar patrones comunes a los dos lados de la comunicación y en
 concreto entre la parte servidora y el juego en modo standalone. En el caso del cliente, la parte en la que accede a
  la sesión para obtener información o para modificar un estado del juego, la interfaz de comunicaciones se encarga
   de pedirle al servidor esa información o que modifique ese estado.
* Para implementar las comunicaciones se ha usado una factoría a la que le incluimos un creador de Sockets. La
 interfaz de comunicaciones es eso, una interfaz. En el ejemplo se ha implementado con Sockets, pero a través de la
  interfaz `SocketCreator` podemos implementar la tecnología de comunicaciones que decidamos. De esta forma
   desacoplamos a toda la distribución de esta tecnología. Y la podremos cambiar desde la creación de las clases
    Cliente y Servidora del juego. 

## Prototipo de Interfaz

```
----- MASTERMIND -----

0 attempt(s):
xxxx
Propose a combination: rybgpo
Wrong proposed combination length
Propose a combination: rybÑ
Wrong colors, they must be: rbygop
Propose a combination: rybo

1 attempt(s):
xxxx
rybo --> 1 blacks and 1 whites
Propose a combination: byro

2 attempt(s):
xxxx
rybo --> 1 blacks and 1 whites
byro --> 2 blacks and 0 whites
Propose a combination: pgro

3 attempt(s):
xxxx
rybo --> 1 blacks and 1 whites
byro --> 2 blacks and 0 whites
pgro --> 4 blacks and 0 whites
You've won!!! ;-)
RESUME? (y/n): y
```


## Ejemplo de uso Standalone:

* Primero debemos compilar y generar el código.

`````shell script
mvn clean install
`````

* Después podremos jugar invocando de la siguiente forma:

````shell script
java -cp target/mastermind-*.jar org.eyo.mastermind.ConsoleMastermind
````

Ejemplo:

````shell script
➜  mastermind-basica-1 git:(documentView) ✗ java -cp target/mastermind-*.jar org.eyo.mastermind.ConsoleMastermind
--- MASTERMIND ---

0 attempt(s):
xxxx

orby
Propose a combination:
obry
1 attempt(s):
xxxx
obry --> 2 blacks and 2 whites

Propose a combination:
broy
2 attempt(s):
xxxx
obry --> 2 blacks and 2 whites
broy --> 2 blacks and 2 whites

Propose a combination:
oo
Wrong proposed combination length
Propose a combination:
ooas
Wrong colors, they must be: rbygop
Propose a combination:
orby
3 attempt(s):
xxxx
obry --> 2 blacks and 2 whites
broy --> 2 blacks and 2 whites
orby --> 4 blacks and 0 whites

You have won
RESUME? (y/n):
n


--- MASTERMIND ---

0 attempt(s):
xxxx

bpoo
Propose a combination:
bpoo
1 attempt(s):
xxxx
bpoo --> 4 blacks and 0 whites

You have won
RESUME? (y/n):
n


--- MASTERMIND ---

0 attempt(s):
xxxx

oyrp
Propose a combination:
oyrp
1 attempt(s):
xxxx
oyrp --> 4 blacks and 0 whites

You have won
RESUME? (y/n):
y

````

## Ejemplo de uso Cliente/Servidor:

* Primero debemos compilar y generar el código.

`````shell script
mvn clean install
`````

* Después deberemos invocar primero al servidor y después interactuar con el cliente:

````shell script
java -cp target/mastermind-*-jar-with-dependencies.jar org.eyo.mastermind.distributed.MastermindServer
java -cp target/mastermind-*-jar-with-dependencies.jar org.eyo.mastermind.distributed.MastermindClient
````

Ejemplo en el cliente:

````shell script
➜  mastermind-basica-1 git:(11.4.mvp.pm.withProxy) java -cp target/mastermind-*-jar-with-dependencies.jar org.eyo.mastermind.distributed.MastermindClient
Cliente> Establecida conexion
--- MASTERMIND ---
****

----- Choose one option -----
1) Propose a combination
Choose an option: 
1
Propose a combination: 
bbbb

1 attempt(s): 
****
bbbb --> 0 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
Choose an option: 
1
Propose a combination: 
bbbb

2 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
yyyy

3 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
gggg

4 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
gggg --> 0 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
yrrr

5 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
gggg --> 0 blacks and 0 whites
yrrr --> 0 blacks and 4 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
rryr

6 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
gggg --> 0 blacks and 0 whites
yrrr --> 0 blacks and 4 whites
rryr --> 2 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
rrry

7 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
gggg --> 0 blacks and 0 whites
yrrr --> 0 blacks and 4 whites
rryr --> 2 blacks and 0 whites
rrry --> 1 blacks and 1 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
1
Propose a combination: 
ryry

4 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
ryry --> 1 blacks and 2 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1   
Propose a combination: 
gryo

5 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
ryry --> 1 blacks and 2 whites
gryo --> 1 blacks and 2 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
bbyb

6 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
ryry --> 1 blacks and 2 whites
gryo --> 1 blacks and 2 whites
bbyb --> 1 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
1
Propose a combination: 
rryr

4 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
rbyb

5 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
rbyb --> 2 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
royg

6 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
rbyb --> 2 blacks and 0 whites
royg --> 3 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
rbyo        

7 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
rbyb --> 2 blacks and 0 whites
royg --> 3 blacks and 0 whites
rbyo --> 2 blacks and 1 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
royb

8 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
rbyb --> 2 blacks and 0 whites
royg --> 3 blacks and 0 whites
rbyo --> 2 blacks and 1 whites
royb --> 3 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 


----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
2

----- Choose one option -----
1) Propose a combination
2) Undo previous action
3) Redo previous action
Choose an option: 
1
Propose a combination: 
royb

5 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
royb --> 3 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
royg

6 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
royb --> 3 blacks and 0 whites
royg --> 3 blacks and 0 whites

----- Choose one option -----
1) Propose a combination
2) Undo previous action
Choose an option: 
1
Propose a combination: 
royp

7 attempt(s): 
****
bbbb --> 0 blacks and 0 whites
bbbb --> 0 blacks and 0 whites
yyyy --> 1 blacks and 2 whites
rryr --> 2 blacks and 0 whites
royb --> 3 blacks and 0 whites
royg --> 3 blacks and 0 whites
royp --> 4 blacks and 0 whites
You've won!!! ;-)
RESUME? (Y/n): 
n
````



[1]: https://en.wikipedia.org/wiki/Mastermind_(board_game)
[2]: https://www.youtube.com/watch?v=2-hTeg2M6GQ
[3]: https://github.com/apecr/designPatterns/tree/master/src/main/java/usantatecla/Composite/compositeFine
[4]: https://github.com/apecr/designPatterns/tree/master/src/main/java/usantatecla/Command/commandFine
[5]: https://github.com/apecr/designPatterns/tree/master/src/main/java/usantatecla/Memento/mementoFine