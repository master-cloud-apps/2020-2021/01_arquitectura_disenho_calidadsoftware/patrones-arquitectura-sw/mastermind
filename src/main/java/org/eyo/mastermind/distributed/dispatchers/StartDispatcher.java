package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.StartUseCaseController;

public class StartDispatcher extends Dispatcher {
    public StartDispatcher(StartUseCaseController startController) {
        super(startController);
    }

    @Override
    public void dispatch() {
        ((StartUseCaseController)this.acceptorController).start();
    }
}
