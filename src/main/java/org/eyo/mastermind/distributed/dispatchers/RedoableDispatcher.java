package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class RedoableDispatcher extends Dispatcher {
    public RedoableDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        boolean redoable = ((ProposalUseCaseController) this.acceptorController).redoable();
        this.mastermindCommunicationResolver.send(redoable);
    }
}
