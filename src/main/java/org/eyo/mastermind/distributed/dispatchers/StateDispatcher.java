package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class StateDispatcher extends Dispatcher {
    public StateDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        this.mastermindCommunicationResolver.send(this.acceptorController.getValueState().ordinal());
    }
}
