package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class RedoDispatcher extends Dispatcher {
    public RedoDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        ((ProposalUseCaseController) this.acceptorController).redo();
    }
}
