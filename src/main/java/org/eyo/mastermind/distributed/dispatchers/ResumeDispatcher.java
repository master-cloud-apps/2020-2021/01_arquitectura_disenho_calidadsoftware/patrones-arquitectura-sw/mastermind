package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ResumeUseCaseController;

public class ResumeDispatcher extends Dispatcher {
    public ResumeDispatcher(ResumeUseCaseController resumeUseCaseController) {
        super(resumeUseCaseController);
    }

    @Override
    public void dispatch() {
        boolean newGame = this.mastermindCommunicationResolver.receiveBoolean();
        ((ResumeUseCaseController)this.acceptorController).clear(newGame);
    }
}
