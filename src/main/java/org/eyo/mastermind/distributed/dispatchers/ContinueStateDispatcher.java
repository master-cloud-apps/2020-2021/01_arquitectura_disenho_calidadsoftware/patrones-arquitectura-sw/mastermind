package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class ContinueStateDispatcher extends Dispatcher {
    public ContinueStateDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        ((ProposalUseCaseController)this.acceptorController).continueState();
    }
}
