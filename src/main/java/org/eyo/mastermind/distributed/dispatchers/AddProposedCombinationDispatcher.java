package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.models.ProposedCombination;

public class AddProposedCombinationDispatcher extends Dispatcher {
    public AddProposedCombinationDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        String proposedCombination = this.mastermindCommunicationResolver.receiveLine();
        ((ProposalUseCaseController) this.acceptorController).addProposedCombination(new ProposedCombination(proposedCombination));
    }
}
