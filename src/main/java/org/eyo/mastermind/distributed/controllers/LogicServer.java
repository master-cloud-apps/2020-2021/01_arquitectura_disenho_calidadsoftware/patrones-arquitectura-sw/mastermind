package org.eyo.mastermind.distributed.controllers;

import org.eyo.mastermind.controllers.LogicStandalone;
import org.eyo.mastermind.distributed.dispatchers.*;

public class LogicServer extends LogicStandalone {

    public void createDispatchers(DispatcherPrototype dispatcherPrototype) {
        dispatcherPrototype.add(FrameType.START, new StartDispatcher(this.startController));
        dispatcherPrototype.add(FrameType.STATE, new StateDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.UNDO, new UndoDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.REDO, new RedoDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.UNDOABLE, new UndoableDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.REDOABLE, new RedoableDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.IS_LOOSER, new IsLooserDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.IS_WINNER, new IsWinnerDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.GET_RESULT_SIZE, new GetResultSizeDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.GET_RESULTS_STRING,
                new GetResultsStringDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.ADD_PROPOSED_COMBINATION,
                new AddProposedCombinationDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.CONTINUE_STATE, new ContinueStateDispatcher(this.proposalUseCaseController));
        dispatcherPrototype.add(FrameType.NEW_GAME, new ResumeDispatcher(this.resumeController));
    }
}
