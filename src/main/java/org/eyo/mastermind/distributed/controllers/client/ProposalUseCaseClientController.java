package org.eyo.mastermind.distributed.controllers.client;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Result;

import java.util.Collections;
import java.util.List;

public class ProposalUseCaseClientController extends ProposalUseCaseController {

    private MastermindCommunicationResolver netWorkResolver;

    public ProposalUseCaseClientController(GameSession session, MastermindCommunicationResolver netWorkResolver) {
        super(session);
        this.netWorkResolver = netWorkResolver;
    }

    @Override
    public void addProposedCombination(Combination proposedCombination) {
        this.netWorkResolver.send(FrameType.ADD_PROPOSED_COMBINATION.name());
        this.netWorkResolver.send(proposedCombination.getsCombination());
    }

    @Override
    public List<Result> getResults() {
        return Collections.emptyList();
    }

    @Override
    public String getResultsAsString() {
        this.netWorkResolver.send(FrameType.GET_RESULTS_STRING.name());
        return this.netWorkResolver.receiveLine();
    }

    @Override
    public int getResultsSize() {
        this.netWorkResolver.send(FrameType.GET_RESULT_SIZE.name());
        return this.netWorkResolver.receiveInt();
    }

    @Override
    public boolean isWinner() {
        this.netWorkResolver.send(FrameType.IS_WINNER.name());
        return this.netWorkResolver.receiveBoolean();
    }

    @Override
    public boolean isLooser() {
        this.netWorkResolver.send(FrameType.IS_LOOSER.name());
        return this.netWorkResolver.receiveBoolean();
    }

    @Override
    public void continueState() {
        this.netWorkResolver.send(FrameType.CONTINUE_STATE.name());
    }

    @Override
    public boolean undoable() {
        this.netWorkResolver.send(FrameType.UNDOABLE.name());
        return this.netWorkResolver.receiveBoolean();
    }

    @Override
    public boolean redoable() {
        this.netWorkResolver.send(FrameType.REDOABLE.name());
        return this.netWorkResolver.receiveBoolean();
    }

    @Override
    public void undo() {
        this.netWorkResolver.send(FrameType.UNDO.name());
    }

    @Override
    public void redo() {
        this.netWorkResolver.send(FrameType.REDO.name());
    }
}
