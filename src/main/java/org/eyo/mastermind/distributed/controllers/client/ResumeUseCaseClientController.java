package org.eyo.mastermind.distributed.controllers.client;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.eyo.mastermind.models.GameSession;

public class ResumeUseCaseClientController extends ResumeUseCaseController {

    private MastermindCommunicationResolver netWorkResolver;

    public ResumeUseCaseClientController(GameSession session, MastermindCommunicationResolver netWorkResolver) {
        super(session);
        this.netWorkResolver = netWorkResolver;
    }

    @Override
    public void clear(boolean newGame) {
        this.netWorkResolver.send(FrameType.NEW_GAME.name());
        this.netWorkResolver.send(newGame);
    }
}
