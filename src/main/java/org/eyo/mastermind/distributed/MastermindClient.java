package org.eyo.mastermind.distributed;

import org.eyo.communications.NetworkFactory;
import org.eyo.communications.tcpip.TcpIpSocketCreator;
import org.eyo.mastermind.Mastermind;
import org.eyo.mastermind.communications.NetworkResolver;
import org.eyo.mastermind.controllers.Logic;
import org.eyo.mastermind.distributed.controllers.client.LogicClient;
import org.eyo.mastermind.views.View;
import org.eyo.mastermind.views.console.ConsoleView;

public class MastermindClient extends Mastermind {

    @Override
    protected Logic createLogic() {
        NetworkFactory tcpipNetworkFactory = new NetworkFactory(new TcpIpSocketCreator());
        return new LogicClient(new NetworkResolver(tcpipNetworkFactory, false));
    }

    @Override
    protected View createView(Logic logic) {
        return new ConsoleView();
    }

    public static void main(String[] args){
        new MastermindClient().play();
    }
}
