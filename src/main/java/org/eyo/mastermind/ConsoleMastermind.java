package org.eyo.mastermind;

import org.eyo.mastermind.controllers.Logic;
import org.eyo.mastermind.controllers.LogicStandalone;
import org.eyo.mastermind.views.View;
import org.eyo.mastermind.views.console.ConsoleView;

class ConsoleMastermind extends Mastermind{


    public static void main(String[] args) {
        new ConsoleMastermind().play();
    }

    @Override
    protected Logic createLogic() {
        return new LogicStandalone();
    }

    @Override
    protected View createView(Logic logic) {
        return new ConsoleView();
    }
}
