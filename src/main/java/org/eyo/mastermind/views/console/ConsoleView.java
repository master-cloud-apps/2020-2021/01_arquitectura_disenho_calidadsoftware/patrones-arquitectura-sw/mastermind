package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.controllers.StartUseCaseController;
import org.eyo.mastermind.views.ProposalInteractView;
import org.eyo.mastermind.views.ResumeInteractView;
import org.eyo.mastermind.views.View;

public class ConsoleView extends View {

    private ResumeInteractView resumeView;
    private ProposalInteractView proposalView;
    private StartView startView;

    public ConsoleView() {
        this.startView = new StartView();
        this.setProposalView(new ProposalView());
        this.setResumeView(new ResumeView());
    }

    public void setResumeView(ResumeInteractView resumeView) {
        this.resumeView = resumeView;
    }
    public void setProposalView(ProposalInteractView proposalView) {
        this.proposalView = proposalView;
    }

    @Override
    public void visit(StartUseCaseController startController) {
        this.startView.interact(startController);
    }

    @Override
    public void visit(ProposalUseCaseController proposalController) {
        this.proposalView.interact(proposalController);
    }

    @Override
    public void visit(ResumeUseCaseController resumeController) {
        this.resumeView.interact(resumeController);
    }
}
