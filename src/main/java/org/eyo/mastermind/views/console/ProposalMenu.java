package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.utils.Menu;

public class ProposalMenu extends Menu {
    public ProposalMenu(ProposalUseCaseController proposalController) {
        super();
        this.addCommand(new ProposalConsoleViewCommand(proposalController));
        this.addCommand(new UndoConsoleViewCommand(proposalController));
        this.addCommand(new RedoConsoleViewCommand(proposalController));
    }

    @Override
    public void setAsker(Asker asker){
        super.setAsker(asker);
        this.getCommands().stream().forEach(command -> ((ConsoleViewCommand) command).setAsker(asker));
    }
}
