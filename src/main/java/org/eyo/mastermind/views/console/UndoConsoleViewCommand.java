package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class UndoConsoleViewCommand extends ConsoleViewCommand {
    public UndoConsoleViewCommand(ProposalUseCaseController proposalController) {
        super(MessageView.UNDO_COMMAND.getMessage(), proposalController);
    }

    @Override
    protected void execute() {
        this.proposalController.undo();
    }

    @Override
    protected boolean isActive() {
        return this.proposalController.undoable();
    }


}
