package org.eyo.mastermind.views.console;

import org.eyo.mastermind.models.Result;
import org.eyo.mastermind.utils.WithConsoleView;

public class ResultView extends WithConsoleView {
    private Result result;

    public ResultView(Result result) {
        super();
        this.result = result;
    }

    public String write() {
        this.console.write(this.result.getStringResult());
        return this.result.getStringResult();
    }

    public ResultView setResult(Result result) {
        this.result = result;
        return this;
    }
}
