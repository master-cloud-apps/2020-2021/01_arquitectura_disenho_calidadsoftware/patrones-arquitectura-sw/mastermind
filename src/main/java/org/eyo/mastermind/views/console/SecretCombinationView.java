package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.UseCaseController;
import org.eyo.mastermind.utils.WithConsoleView;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SecretCombinationView extends WithConsoleView {

    public String writeEncryptedSecret(UseCaseController useCaseController) {
        writeSecretInConsole(useCaseController);
        return Arrays.stream(this.getListStringCombination(useCaseController))
                .map(e -> MessageView.SECRET.getMessage())
                .collect(Collectors.joining(""));
    }

    private void writeSecretInConsole(UseCaseController useCaseController) {
        Arrays.stream(this.getListStringCombination(useCaseController))
                .forEach(e -> MessageView.SECRET.write());
        this.console.writeln();
    }

    private String[] getListStringCombination(UseCaseController useCaseController) {
        return useCaseController.getSecretCombination().getsCombination().split("");
    }
}
