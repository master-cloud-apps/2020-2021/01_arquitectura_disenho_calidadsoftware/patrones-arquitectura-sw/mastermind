package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.StartUseCaseController;

public class StartView {

    void interact(StartUseCaseController startController) {
        MessageView.START_GAME_GREETING.writeln();
        StringBuilder startGreeting = new StringBuilder(MessageView.START_GAME_GREETING.getMessage());
        startGreeting.append(MessageView.RETURN.getMessage());
        startGreeting.append(new SecretCombinationView().writeEncryptedSecret(startController));
        startController.start();
    }
}
