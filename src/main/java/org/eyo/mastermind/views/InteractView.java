package org.eyo.mastermind.views;

import org.eyo.mastermind.controllers.UseCaseController;

public interface InteractView {

    boolean interact(UseCaseController useCaseController);
}
