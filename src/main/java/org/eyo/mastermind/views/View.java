package org.eyo.mastermind.views;

import org.eyo.mastermind.controllers.AcceptorController;
import org.eyo.mastermind.controllers.ControllersVisitor;

public abstract class View implements ControllersVisitor {

    public void interact(AcceptorController useCaseController){
        useCaseController.accept(this);
    }

}
