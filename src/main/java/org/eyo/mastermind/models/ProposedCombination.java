package org.eyo.mastermind.models;

/**
 * albertoeyo created on 10/10/2020
 **/
public class ProposedCombination extends Combination {
    public ProposedCombination() {
        super("");
    }

    public ProposedCombination(String sCombination) {
        super(sCombination);
    }
}
