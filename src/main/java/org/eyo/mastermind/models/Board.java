package org.eyo.mastermind.models;

import java.util.ArrayList;
import java.util.List;

/**
 * albertoeyo created on 11/10/2020
 **/
public class Board {

    private List<Result> listResults;
    private List<Combination> proposedCombinations;
    private Combination secretCombination;
    public static final int MAX_TURNS = 10;

    public Board() {
        this.reset();
    }

    public Board(List<Result> listResults, List<Combination> proposedCombinations, Combination secretCombination) {
        this.secretCombination = new ProposedCombination(secretCombination.getsCombination());
        this.proposedCombinations = new ArrayList<>();
        proposedCombinations.stream().forEach(this.proposedCombinations::add);
        this.listResults = new ArrayList<>();
        listResults.stream().forEach(this.listResults::add);
    }

    public List<Result> getResults() {
        return this.listResults;
    }

    public void reset() {
        this.listResults = new ArrayList<>();
        this.proposedCombinations = new ArrayList<>();
        this.setSecretCombination(new SecretCombination());
    }

    public void addProposedCombination(Combination combination) {
        this.proposedCombinations.add(combination);
        this.listResults.add(this.secretCombination.getResultFromProposedCombination(combination));
    }

    public boolean isWinner() {
        if (this.listResults.isEmpty()){
            return false;
        }
        return this.listResults.get(this.listResults.size() -1).isWinner();
    }

    public boolean isLooser() {
        return this.listResults.size() == MAX_TURNS;
    }

    public Combination getProposedCombination(int position) {
        return this.proposedCombinations.get(position);
    }

    public Result getResult(int position) {
        return this.listResults.get(position);
    }

    public Combination getSecretCombination() {
        return this.secretCombination;
    }

    public void setSecretCombination(Combination secretCombination) {
        this.secretCombination = secretCombination;
    }

    public List<Combination> getProposedCombinations() {
        return proposedCombinations;
    }

    public Board copy() {
        return new Board(this.listResults, this.proposedCombinations, this.secretCombination);
    }

    public Memento createMemento() {
        return new Memento(this);
    }

    public void set(Memento memento) {
        this.secretCombination = memento.getBoard().getSecretCombination();
        this.proposedCombinations = new ArrayList<>();
        memento.getBoard().getProposedCombinations().stream().forEach(this.proposedCombinations::add);
        this.listResults = new ArrayList<>();
        memento.getBoard().getResults().stream().forEach(this.listResults::add);
    }
}
