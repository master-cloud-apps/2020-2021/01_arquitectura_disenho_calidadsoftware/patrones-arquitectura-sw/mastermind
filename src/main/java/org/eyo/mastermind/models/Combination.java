package org.eyo.mastermind.models;

import java.util.Arrays;

public abstract class Combination {

    private String sCombination;

    protected Combination(String sCombination) {
        this.sCombination = sCombination;
    }

    public String[] getAllowedCharacters() {
        return allowedCharacters;
    }

    private String[] allowedCharacters = {"r", "b", "y", "g", "o", "p"};

    public static final String WRONG_PROPOSED_COMBINATION_LENGTH = "Wrong proposed combination length";
    public static final String WRONG_COLORS_THEY_MUST_BE = "Wrong colors, they must be: ";

    public static final int COMBINATION_SIZE = 4;

    private boolean isRightLength(){
        return sCombination.length() == COMBINATION_SIZE;
    }

    private String getWrongColorErrorMessage(){
        return WRONG_COLORS_THEY_MUST_BE.concat(String.join("", allowedCharacters));
    }

    private boolean areCharactersAllowed(){
        for (String element : sCombination.split("")){
            if (!Arrays.asList(allowedCharacters).contains(element))
                return false;
        }
        return true;
    }


    private boolean isCorrect() {
        return this.isRightLength() && areCharactersAllowed();
    }

    private String getReason(){
        if (!this.isRightLength())
            return WRONG_PROPOSED_COMBINATION_LENGTH;
        if (!this.areCharactersAllowed())
            return this.getWrongColorErrorMessage();
        return "";
    }

    public CombinationStatus check() {
        return new CombinationStatus(this.isCorrect(), this.getReason());
    }

    public String setCombination(String sCombination){
        this.sCombination = sCombination;
        return this.sCombination;
    }

    public String getsCombination() {
        return sCombination;
    }

    public Result getResultFromProposedCombination(Combination combination) {
        Result result = new Result(0, 0, combination.getsCombination());
        String[] aCombination = combination.getsCombination().split("");
        String[] aSecretCombination = this.getsCombination().split("");
        for(int i = 0; i < aCombination.length; i++) {
            if (aCombination[i].equals(aSecretCombination[i])){
                result.setBlacks(result.getBlacks() + 1);
                aCombination[i] = "x";
                aSecretCombination[i] = "x";
            }else if (Arrays.toString(aSecretCombination).contains(aCombination[i])){
                result.setWhites(result.getWhites() + 1);
            }
        }
        return result;
    }

}
