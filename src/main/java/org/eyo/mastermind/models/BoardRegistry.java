package org.eyo.mastermind.models;

import java.util.ArrayList;
import java.util.List;

public class BoardRegistry {

    private List<Memento> mementoList;
    private Board board;

    private int firstPrevious;

    public BoardRegistry(Board board) {
        this.board = board;
        this.reset();
    }

    void reset() {
        this.mementoList = new ArrayList<>();
        this.firstPrevious = 0;
    }

    void registry() {
        for (int i = 0; i < this.firstPrevious; i++) {
            this.mementoList.remove(0);
        }
        this.firstPrevious = 0;
        this.mementoList.add(this.firstPrevious, this.board.createMemento());
    }

    void undo() {
        this.firstPrevious++;
        this.board.set(this.mementoList.get(this.firstPrevious));
    }

    void redo() {
        this.firstPrevious--;
        this.board.set(this.mementoList.get(this.firstPrevious));
    }

    boolean undoable() {
        return this.firstPrevious < this.mementoList.size() - 1;
    }

    boolean redoable() {
        return this.firstPrevious >= 1;
    }
}
