package org.eyo.mastermind.models;

import java.security.SecureRandom;
import java.util.stream.Stream;

/**
 * albertoeyo created on 10/10/2020
 **/
public class SecretCombination extends Combination {

    private SecureRandom random = new SecureRandom();

    public SecretCombination() {
        super("");
        this.setCombination(this.generateRandomCombination());
    }

    private String generateRandomCombination() {
        return Stream
                .generate(() -> this.random.nextInt(6))
                .limit(4)
                .map(randomInt -> this.getAllowedCharacters()[randomInt])
                .reduce("", String::concat);
    }

}
