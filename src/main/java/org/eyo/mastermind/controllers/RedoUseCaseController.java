package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Session;

public class RedoUseCaseController extends UseCaseController{

    private final Session session;

    public RedoUseCaseController(GameSession sessionImpl) {
        super(sessionImpl);
        this.session = ((Session)this.sessionImpl);
    }

    public void redo() {
        this.session.redo();
    }

    public boolean redoable() {
        return this.session.redoable();
    }
}
