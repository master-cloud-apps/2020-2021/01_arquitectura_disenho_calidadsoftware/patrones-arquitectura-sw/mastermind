package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Session;

public class UndoUseCaseController extends UseCaseController{

    private final Session session;

    public UndoUseCaseController(GameSession sessionImpl) {
        super(sessionImpl);
        this.session = ((Session)this.sessionImpl);
    }

    public void undo() {
        this.session.undo();
    }

    public boolean undoable() {
        return this.session.undoable();
    }
}
