package org.eyo.mastermind.controllers.server;

import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Session;

public class ResumeUseCaseServerController extends ResumeUseCaseController {

    private final Session session;

    public ResumeUseCaseServerController(GameSession sessionImpl) {
        super(sessionImpl);
        this.session = ((Session)this.sessionImpl);
    }

    @Override
    public void clear(boolean newGame) {
        if (newGame) {
            this.session.reset();
        } else {
            this.session.next();
        }
    }
}
