package org.eyo.mastermind.controllers.server;

import org.eyo.mastermind.controllers.PlayController;
import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.RedoUseCaseController;
import org.eyo.mastermind.controllers.UndoUseCaseController;
import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Result;

import java.util.List;

public class ProposalUseCaseServerController extends ProposalUseCaseController {

    private UndoUseCaseController undoController;
    private RedoUseCaseController redoController;
    private PlayController playController;

    public ProposalUseCaseServerController(GameSession sessionImpl) {
        super(sessionImpl);
        this.undoController = new UndoUseCaseController(sessionImpl);
        this.redoController = new RedoUseCaseController(sessionImpl);
        this.playController = new PlayController(sessionImpl);
    }

    @Override
    public void addProposedCombination(Combination proposedCombination) {
        this.playController.addProposedCombination(proposedCombination);
    }

    @Override
    public List<Result> getResults() {
        return this.playController.getResults();
    }

    @Override
    public String getResultsAsString() {
        return this.playController.getResultsAsString();
    }

    @Override
    public int getResultsSize() {
        return this.playController.getResultsSize();
    }

    @Override
    public boolean isWinner() {
        return this.playController.isWinner();
    }

    @Override
    public boolean isLooser() {
        return this.playController.isLooser();
    }

    @Override
    public void continueState() {
        this.playController.next();
    }

    @Override
    public boolean undoable() {
        return this.undoController.undoable();
    }

    @Override
    public boolean redoable() {
        return this.redoController.redoable();
    }

    @Override
    public void undo() {
        this.undoController.undo();
    }

    @Override
    public void redo() {
        this.redoController.redo();
    }
}
