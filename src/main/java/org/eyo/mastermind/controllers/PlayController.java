package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Result;
import org.eyo.mastermind.models.Session;

import java.util.List;

public class PlayController extends UseCaseController {

    private Session session;

    public PlayController(GameSession sessionImpl) {
        super(sessionImpl);
        this.session = ((Session)this.sessionImpl);
    }

    public List<Result> getResults() {
        return this.session.getResults();
    }

    public boolean isWinner() {
        return this.session.isWinner();
    }

    public boolean isLooser() {
        return this.session.isLooser();
    }

    public void next() {
        this.session.next();
    }

    public void addProposedCombination(Combination proposedCombination) {
        this.session.addProposedCombination(proposedCombination);
    }

    public String getResultsAsString() {
        return this.getResults()
                .stream()
                .map(Result::getStringResult)
                .reduce("", (acc, result) -> acc + result);
    }

    public int getResultsSize() {
        return this.getResults().size();
    }
}
