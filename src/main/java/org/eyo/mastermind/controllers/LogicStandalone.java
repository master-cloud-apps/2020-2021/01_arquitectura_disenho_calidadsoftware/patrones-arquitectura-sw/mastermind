package org.eyo.mastermind.controllers;

import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.controllers.server.ResumeUseCaseServerController;
import org.eyo.mastermind.controllers.server.StartUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.types.StateValue;

public class LogicStandalone extends Logic{

    protected StartUseCaseController startController;
    protected ProposalUseCaseController proposalUseCaseController;
    protected ResumeUseCaseController resumeController;

    public LogicStandalone(){
        this.sessionImpl = new Session();
        this.startController = new StartUseCaseServerController(this.sessionImpl);
        this.proposalUseCaseController = new ProposalUseCaseServerController(this.sessionImpl);
        this.resumeController = new ResumeUseCaseServerController(this.sessionImpl);
        this.controllers.put(StateValue.INITIAL, this.startController);
        this.controllers.put(StateValue.IN_GAME, this.proposalUseCaseController);
        this.controllers.put(StateValue.RESUME, this.resumeController);
        this.controllers.put(StateValue.EXIT, null);
    }

}