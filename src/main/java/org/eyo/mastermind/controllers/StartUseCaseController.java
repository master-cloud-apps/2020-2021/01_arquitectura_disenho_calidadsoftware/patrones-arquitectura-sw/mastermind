package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.GameSession;

public abstract class StartUseCaseController extends UseCaseController implements AcceptorController {

    protected StartUseCaseController(GameSession sessionImpl) {
        super(sessionImpl);
    }

    @Override
    public void accept(ControllersVisitor controllersVisitor) {
        controllersVisitor.visit(this);
    }

    public abstract void start();

}
