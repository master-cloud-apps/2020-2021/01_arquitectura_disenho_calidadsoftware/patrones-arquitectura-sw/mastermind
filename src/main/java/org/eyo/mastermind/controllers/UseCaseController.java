package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.Board;
import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.types.StateValue;

public abstract class UseCaseController{

    protected GameSession sessionImpl;

    protected UseCaseController(GameSession sessionImpl) {
        this.sessionImpl = sessionImpl;
    }


    public Combination getSecretCombination(){
        return this.sessionImpl.getSecretCombination();
    }
    public Board getBoard() {
        return this.sessionImpl.getBoard();
    }
    public StateValue getValueState() {
        return this.sessionImpl.getStateValue();
    }
}
