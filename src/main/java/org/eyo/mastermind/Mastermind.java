package org.eyo.mastermind;

import org.eyo.mastermind.controllers.AcceptorController;
import org.eyo.mastermind.controllers.Logic;
import org.eyo.mastermind.views.View;

public abstract class Mastermind {

    private Logic logic;
    private View view;

    protected Mastermind() {
        this.logic = this.createLogic();
        this.view = this.createView(this.logic);
    }

    protected abstract Logic createLogic();

    protected abstract View createView(Logic logic);

    protected boolean play() {
        AcceptorController useCaseController;
        do {
            useCaseController = this.logic.getController();
            if (useCaseController != null) {
                this.view.interact(useCaseController);
            }
        } while (useCaseController != null);
        return true;
    }

    public void setView(View view) {
        this.view = view;
    }

    public Logic getLogic() {
        return logic;
    }
}
