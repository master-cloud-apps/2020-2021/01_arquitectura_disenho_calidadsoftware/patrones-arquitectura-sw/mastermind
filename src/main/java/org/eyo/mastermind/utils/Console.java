package org.eyo.mastermind.utils;

import java.io.PrintStream;

/**
 * albertoeyo created on 12/10/2020
 **/
public class Console {

    private static Console console;

    public static Console instance() {
        if (Console.console == null) {
            Console.console = new Console();
        }
        return Console.console;
    }

    public void writeln(String message) {
        getPrintStream().println(message);
    }

    public void write(String message) {
        getPrintStream().print(message);
    }

    public void writeln() {
        getPrintStream().println();
    }

    private PrintStream getPrintStream(){
        return System.out;
    }
}
