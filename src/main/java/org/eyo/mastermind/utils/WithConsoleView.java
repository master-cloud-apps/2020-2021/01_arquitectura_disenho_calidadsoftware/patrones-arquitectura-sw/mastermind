package org.eyo.mastermind.utils;

public abstract class WithConsoleView {
    protected Console console;

    protected WithConsoleView() {
        this.console = Console.instance();
    }
}
