package org.eyo.mastermind.utils;

import java.util.Scanner;

/**
 * albertoeyo created on 11/10/2020
 **/
public class ConsoleAsker implements Asker {

    private Console console = Console.instance();

    @Override
    public String ask(String questionMessage) {
        this.console.writeln(questionMessage);
        return new Scanner(System.in).nextLine();
    }
}
