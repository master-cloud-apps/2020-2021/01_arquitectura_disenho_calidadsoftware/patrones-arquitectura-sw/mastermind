package org.eyo.mastermind.utils;

import org.eyo.mastermind.views.console.MessageView;

import java.util.ArrayList;
import java.util.List;

public abstract class Menu {

    private static final String OPTION = "----- Choose one option -----";
    private List<Command> commands;
    private Asker asker;
    private Console console = Console.instance();

    protected Menu() {
        this.commands = new ArrayList<>();
        this.asker = new ConsoleAsker();
    }

    public List<Command> getCommands() {
        return this.commands;
    }

    protected void addCommand(Command command){
        this.commands.add(command);
    }

    public void execute(){
        int option;
        do {
            printMenuOptions();
            option = this.askForMunber() - 1;
        } while (!isOptionAvailable(option));
        getActiveCommands().get(option).execute();
    }

    private List<Command> getActiveCommands(){
        ArrayList<Command> activeCommands = new ArrayList<>();
        for (int i = 0; i < this.commands.size(); i++) {
            if (this.commands.get(i).isActive()) {
                activeCommands.add(this.commands.get(i));
            }
        }
        return activeCommands;
    }

    private void printMenuOptions() {
        console.writeln();
        console.writeln(Menu.OPTION);
        for (int i = 0; i < getActiveCommands().size(); i++) {
            console.writeln((i + 1) + ") " + getActiveCommands().get(i).getTitle());
        }
    }

    private boolean isOptionAvailable(int option) {
        return new ClosedInterval(0, getActiveCommands().size() - 1).isIncluded(option);
    }

    private int askForMunber(){
        String sNumber = this.asker.ask(MessageView.CHOOSE_OPTION.getMessage());
        if (!isNumeric(sNumber)){
            return -1;
        }
        return Integer.parseInt(sNumber);
    }

    private boolean isNumeric(String input){
        if (input == null) {
            return false;
        }
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void setAsker(Asker asker) {
        this.asker = asker;
    }
}
