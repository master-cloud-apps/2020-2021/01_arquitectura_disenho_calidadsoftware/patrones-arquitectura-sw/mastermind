package org.eyo.mastermind.communications;

import org.apache.log4j.Logger;
import org.eyo.communications.CommunicationsResolver;
import org.eyo.communications.NetworkFactory;

public class NetworkResolver implements MastermindCommunicationResolver {

    private static final Logger logger = Logger.getLogger(NetworkResolver.class);

    public NetworkResolver(NetworkFactory networkFactory, boolean isServer) {
        if (isServer) {
            this.communicationsResolver = networkFactory.createCommunicationServerInterface();
        }else{
            this.communicationsResolver = networkFactory.createCommunicationClientInterface();
        }
    }

    private CommunicationsResolver communicationsResolver;

    @Override
    public void close() {
        this.communicationsResolver.close();
    }

    @Override
    public String receiveLine() {
        String lineRecived = this.communicationsResolver.receiveLine();
        logger.info("Line received: "+ lineRecived);
        return lineRecived;
    }

    @Override
    public void send(String name) {
        logger.info("NR - Sendind: " + name);
        this.communicationsResolver.send(name);
    }

    @Override
    public void send(int value) {
        this.send("" + value);
    }

    @Override
    public void send(boolean value) {
        this.send("" + value);
    }

    @Override
    public void send(char value) {
        this.send("" + value);
    }

    @Override
    public int receiveInt() {
        return this.communicationsResolver.receiveInt();
    }

    @Override
    public boolean receiveBoolean() {
        return this.communicationsResolver.receiveBoolean();
    }
}
