package org.eyo.communications;

import org.apache.log4j.Logger;
import org.eyo.communications.tcpip.TCPIP;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class NetworkFactory {

    private static final String LOCAL_HOST = "localhost";
    private static final int LOCAL_PORT = 2020;
    private static final Logger logger = Logger.getLogger(NetworkFactory.class);

    private SocketCreator socketCreator;

    public NetworkFactory(SocketCreator socketCreator) {
        this.socketCreator = socketCreator;
    }

    public CommunicationsResolver createCommunicationClientInterface() {
        Socket socket = this.socketCreator.createSocket(LOCAL_HOST, LOCAL_PORT);
        System.out.println("Cliente> Establecida conexion");
        return new TCPIP(socket);
    }

    public CommunicationsResolver createCommunicationServerInterface() {
        try {
            ServerSocket serverSocket = this.socketCreator.createServerSocket(LOCAL_PORT);
            System.out.println("Servidor> Esperando conexion...");
            Socket socket = serverSocket.accept();
            System.out.println("Servidor> Recibida conexion de " + socket.getInetAddress().getHostAddress() + ":"
                    + socket.getPort());
            return new TCPIP(serverSocket, socket);
        } catch (IOException ex) {
            logger.error(ex);
            return null;
        }
    }
}
