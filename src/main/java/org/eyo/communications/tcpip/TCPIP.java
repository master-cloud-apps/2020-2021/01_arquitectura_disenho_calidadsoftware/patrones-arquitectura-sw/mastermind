package org.eyo.communications.tcpip;

import org.apache.log4j.Logger;
import org.eyo.communications.CommunicationsResolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPIP implements CommunicationsResolver {

    private ServerSocket serverSocket;

    private Socket socket;

    private PrintWriter out;

    private BufferedReader in;
    private static final Logger logger = Logger.getLogger(TCPIP.class);

    public TCPIP(Socket socket) {
        this.serverSocket = null;
        this.socket = socket;
        try {
            this.out = new PrintWriter(socket.getOutputStream());
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception ex) {
            logger.error(ex);
            System.exit(0);
        }
    }

    public TCPIP(ServerSocket serverSocket, Socket socket) {
        this(socket);
        this.serverSocket = serverSocket;
    }

    @Override
    public void send(String value) {
        logger.info("TCPIP - Sending: " + value);
        this.out.println(value);
        this.out.flush();
    }

    @Override
    public String readLine() {
        String result = null;
        try {
            result = this.in.readLine();
            logger.info("TCPIP - Line readed: " +result);
        } catch (IOException e) {
            logger.error("TCPIP - 500: readline");
        }
        return result;
    }

    @Override
    public void close() {
        try {
            this.in.close();
            this.out.close();
            this.socket.close();
            if (this.serverSocket != null) {
                this.serverSocket.close();
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    @Override
    public String receiveLine() {
        String result = null;
        try {
            result = this.in.readLine();
        } catch (IOException e) {
            logger.error("TCPIP - 500: receiveLine");
        }
        return result;
    }

    @Override
    public int receiveInt() {
        int result = -1;
        try {
            String resultString = this.in.readLine();
            result = Integer.parseInt(resultString);
        } catch (IOException e) {
            logger.error(e);
        }
        return result;
    }

    @Override
    public boolean receiveBoolean() {
        boolean result = false;
        try {
            String resultString = this.in.readLine();
            result = Boolean.parseBoolean(resultString);
        } catch (IOException e) {
            logger.error(e);
        }
        return result;
    }
}
