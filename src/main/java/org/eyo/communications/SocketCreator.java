package org.eyo.communications;

import java.net.ServerSocket;
import java.net.Socket;

public interface SocketCreator {

    Socket createSocket(String host, int port);

    ServerSocket createServerSocket(int port);
}
