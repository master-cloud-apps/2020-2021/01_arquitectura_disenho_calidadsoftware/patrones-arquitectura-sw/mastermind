package org.eyo.communications;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class NetworkFactoryTest {

    @InjectMocks
    private NetworkFactory networkFactory;

    @Mock
    private SocketCreator socketCreator;
    @Mock
    private Socket socket;
    @Mock
    private ServerSocket mockServerSocket;
    @Mock
    private OutputStream mockOutputStream;
    @Mock
    private InputStream mockInputStream;
    @Mock
    private InetAddress mockInetAddress;

    @BeforeEach
    void setUp() {
        this.networkFactory = new NetworkFactory(this.socketCreator);
    }

    @Test
    void should_get_a_tcp_ip_client() throws IOException {
        prepareSocketsForTest();

        CommunicationsResolver tcpIpClient = this.networkFactory.createCommunicationClientInterface();

        assertNotNull(tcpIpClient);
    }

    private void prepareSocketsForTest() throws IOException {
        lenient().when(this.socketCreator.createSocket(any(String.class), any(int.class)))
                .thenReturn(this.socket);
        when(this.socket.getOutputStream())
                .thenReturn(this.mockOutputStream);
        when(this.socket.getInputStream())
                .thenReturn(this.mockInputStream);
    }

    @Test
    void should_get_a_tcp_ip_server() throws IOException {
        prepareSocketsForTest();
        when(this.socketCreator.createServerSocket(anyInt()))
                .thenReturn(this.mockServerSocket);
        when(this.mockServerSocket.accept())
                .thenReturn(this.socket);
        when(this.socket.getInetAddress())
                .thenReturn(this.mockInetAddress);

        CommunicationsResolver tcpIpServer = this.networkFactory.createCommunicationServerInterface();

        assertNotNull(tcpIpServer);
    }

    @Test
    void should_fail_when_server_socket_crashes() throws IOException {
        when(this.socketCreator.createServerSocket(anyInt()))
                .thenReturn(this.mockServerSocket);
        when(this.mockServerSocket.accept())
                .thenThrow(new IOException("Test exception"));

        CommunicationsResolver tcpIpServer = this.networkFactory.createCommunicationServerInterface();

        assertNull(tcpIpServer);
    }
}
