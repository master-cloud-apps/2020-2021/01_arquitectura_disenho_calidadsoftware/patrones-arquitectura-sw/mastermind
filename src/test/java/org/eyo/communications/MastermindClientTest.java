package org.eyo.communications;

import org.eyo.mastermind.distributed.MastermindClient;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class MastermindClientTest {

    private MastermindClient mastermindClient;

    @Test
    @Disabled("for demonstration purposes")
    void should_create_element(){
        this.mastermindClient = new MastermindClient();
        assertNotNull(this.mastermindClient);
    }
}
