package org.eyo.mastermind.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClosedIntervalTest {

    @Test
    void should_return_error_bad_interval(){
        assertThrows(IllegalArgumentException.class, () -> new ClosedInterval(0, -1));
        assertThrows(IllegalArgumentException.class, () -> new ClosedInterval(5, 3));
    }

    @Test
    void should_return_is_included(){
        ClosedInterval closedInterval = new ClosedInterval(0, 12);

        assertTrue(closedInterval.isIncluded(6));
        assertFalse(closedInterval.isIncluded(13));
        assertEquals("[0, 12]", closedInterval.toString());
    }
}
