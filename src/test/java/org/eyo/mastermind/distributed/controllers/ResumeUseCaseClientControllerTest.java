package org.eyo.mastermind.distributed.controllers;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.controllers.client.ResumeUseCaseClientController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class ResumeUseCaseClientControllerTest {

    @InjectMocks
    private ResumeUseCaseClientController resumeUseCaseController;

    @Mock
    private MastermindCommunicationResolver communicationResolver;

    @Test
    void should_call_start(){
        this.resumeUseCaseController.clear(true);
        verify(this.communicationResolver).send(true);
    }
}
