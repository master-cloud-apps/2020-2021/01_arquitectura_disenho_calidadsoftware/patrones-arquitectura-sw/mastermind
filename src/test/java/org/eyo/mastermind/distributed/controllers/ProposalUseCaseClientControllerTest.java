package org.eyo.mastermind.distributed.controllers;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.controllers.client.ProposalUseCaseClientController;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.eyo.mastermind.models.ProposedCombination;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class ProposalUseCaseClientControllerTest {

    @InjectMocks
    private ProposalUseCaseClientController proposalUseCaseClientController;

    @Mock
    private MastermindCommunicationResolver communicationResolver;

    @Test
    void should_call_add_proposed_combination(){
        this.proposalUseCaseClientController.addProposedCombination(new ProposedCombination("bbbb"));
        verify(this.communicationResolver).send("bbbb");
    }

    @Test
    void should_call_continue_state(){
        this.proposalUseCaseClientController.continueState();
        verify(this.communicationResolver).send(FrameType.CONTINUE_STATE.name());
    }

    @Test
    void should_call_undo(){
        this.proposalUseCaseClientController.undo();
        verify(this.communicationResolver).send(FrameType.UNDO.name());;
    }

    @Test
    void should_call_redo(){
        this.proposalUseCaseClientController.redo();
        verify(this.communicationResolver).send(FrameType.REDO.name());
    }

    @Test
    void should_call_get_results(){
        assertEquals(0, this.proposalUseCaseClientController.getResults().size());
    }

    @Test
    void should_call_get_results_as_string(){
        when(this.communicationResolver.receiveLine()).thenReturn("yyyy");

        assertEquals("yyyy", this.proposalUseCaseClientController.getResultsAsString());
    }

    @Test
    void should_call_get_results_size(){
        when(this.communicationResolver.receiveInt()).thenReturn(4);

        assertEquals(4, this.proposalUseCaseClientController.getResultsSize());
    }

    @Test
    void should_check_winner(){
        when(this.communicationResolver.receiveBoolean()).thenReturn(true);
        assertTrue(this.proposalUseCaseClientController.isWinner());

        when(this.communicationResolver.receiveBoolean()).thenReturn(false);
        assertFalse(this.proposalUseCaseClientController.isLooser());

        when(this.communicationResolver.receiveBoolean()).thenReturn(false);
        assertFalse(this.proposalUseCaseClientController.undoable());

        when(this.communicationResolver.receiveBoolean()).thenReturn(true);
        assertTrue(this.proposalUseCaseClientController.redoable());
    }
}
