package org.eyo.mastermind.distributed;

import org.eyo.mastermind.distributed.controllers.client.LogicClient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class LogicClientTest {

    private LogicClient logicClient;

    @Test
    void test_not_null(){
        this.logicClient = new LogicClient(null);

        assertNotNull(this.logicClient);
    }
}
