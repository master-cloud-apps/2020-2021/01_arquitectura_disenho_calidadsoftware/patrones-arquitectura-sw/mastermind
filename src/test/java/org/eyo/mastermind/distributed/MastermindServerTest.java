package org.eyo.mastermind.distributed;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class MastermindServerTest {

    @InjectMocks
    private MastermindServer mastermindServer;
    @Mock
    private MastermindCommunicationResolver communicationsResolver;

    @Test
    void should_create_the_mastermind_server() {
        assertNotNull(this.mastermindServer);
    }

    @Test
    void should_execute_start_and_close() {
        when(this.communicationsResolver.receiveLine()).then(new Answer<String>() {
            private int times = 0;

            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (times == 0) {
                    times++;
                    return FrameType.START.toString();
                }
                return null;
            }
        });
        this.mastermindServer.serve();

        verify(this.communicationsResolver).close();
    }

    @Test
    void should_execute_start_is_winner_and_close() {
        when(this.communicationsResolver.receiveLine()).then(new Answer<String>() {
            private int times = 0;
            private FrameType[] frames = {FrameType.START, FrameType.IS_WINNER, FrameType.CLOSE};

            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                times++;
                return frames[times - 1].toString();
            }
        });
        this.mastermindServer.serve();

        verify(this.communicationsResolver).close();
    }

    @Test
    void should_execute_all_states() {
        when(this.communicationsResolver.receiveLine()).then(new Answer<String>() {
            private int times = 0;
            private String[] frames = {FrameType.START.name(), FrameType.STATE.name(), FrameType.UNDOABLE.name(),
                    FrameType.REDOABLE.name(),
                    FrameType.IS_LOOSER.name(), FrameType.IS_WINNER.name(), FrameType.GET_RESULT_SIZE.name(),
                    FrameType.GET_RESULTS_STRING.name(),
                    FrameType.ADD_PROPOSED_COMBINATION.name(),
                    "bbbb", FrameType.ADD_PROPOSED_COMBINATION.name(), "gggg",
                    FrameType.UNDO.name(),
                    FrameType.ADD_PROPOSED_COMBINATION.name(), "gggg", FrameType.ADD_PROPOSED_COMBINATION.name(),
                    "gggg", FrameType.ADD_PROPOSED_COMBINATION.name(), "gggg",
                    FrameType.UNDO.name(), FrameType.REDO.name(),
                    FrameType.NEW_GAME.name(),
                    FrameType.CONTINUE_STATE.name(),
                    FrameType.CLOSE.name()};

            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                times++;
                return frames[times - 1];
            }
        });
        this.mastermindServer.serve();

        verify(this.communicationsResolver).close();
    }
}
