package org.eyo.mastermind.distributed;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.types.StateValue;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class ClientGameSessionTest {

    @InjectMocks
    private ClientGameSession clientGameSession;

    @Mock
    private MastermindCommunicationResolver communicationResolver;

    @Test
    void should_get_state_value(){
        when(this.communicationResolver.receiveInt()).thenReturn(0);

        assertEquals(StateValue.INITIAL, this.clientGameSession.getValueState());
    }

    @Test
    void should_get_secret_combination(){
        assertEquals("bbbb", this.clientGameSession.getSecretCombination().getsCombination());
    }

    @Test
    void should_get_board(){
        assertNull(this.clientGameSession.getBoard());
    }
}
