package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ProposalConsoleViewCommandTest {

    private ProposalConsoleViewCommand proposalCommand;
    private ProposalUseCaseController proposalController;
    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.proposalController = new ProposalUseCaseServerController(this.sessionImpl);
        this.proposalCommand = new ProposalConsoleViewCommand(this.proposalController);
    }

    @Test
    void should_the_command_be_active(){
        assertTrue(this.proposalCommand.isActive());
    }
}
