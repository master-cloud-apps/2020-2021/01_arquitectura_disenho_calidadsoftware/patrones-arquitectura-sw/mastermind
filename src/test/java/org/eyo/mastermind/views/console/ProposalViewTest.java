package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.models.ProposedCombination;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.utils.Asker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProposalViewTest {

    private ProposalView proposalView;
    private ProposalUseCaseController proposalController;
    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.proposalController = new ProposalUseCaseServerController(this.sessionImpl);
        this.proposalView = new ProposalView();
    }

    @Test
    void should_include_one_combination_and_win_the_game(){
        String boardCombination = "bbbb";
        this.proposalView.setAsker(new Asker() {
            int times = 0;
            @Override
            public String ask(String questionMessage) {
                if (times == 0){
                    times++;
                    return "1";
                }
                return boardCombination;
            }
        });
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination(boardCombination));

        this.proposalView.interact(this.proposalController);

        assertTrue(this.proposalController.isWinner());
    }

    @Test
    void should_include_one_wrong_combinations_and_not_win_the_game(){
        String boardCombination = "bbbb";
        this.proposalView.setAsker(new Asker() {
            int times = 0;
            @Override
            public String ask(String questionMessage) {
                if (times == 0){
                    times++;
                    return "1";
                }
                return "rrrr";
            }
        });
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination(boardCombination));

        this.proposalView.interact(this.proposalController);

        assertFalse(this.proposalController.isLooser());
        assertFalse(this.proposalController.isWinner());
    }

    @Test
    void should_include_ten_wrong_combinations_and_not_win_the_game(){
        String boardCombination = "bbbb";
        this.proposalView.setAsker(new Asker() {
            int times = 0;
            @Override
            public String ask(String questionMessage) {
                if (times % 2 == 0){
                    times++;
                    return "1";
                }
                times++;
                return "rrrr";
            }
        });
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination(boardCombination));

        IntStream.range(0, 10).forEach(time -> this.proposalView.interact(this.proposalController));

        assertTrue(this.proposalController.isLooser());
        assertFalse(this.proposalController.isWinner());
    }
}
