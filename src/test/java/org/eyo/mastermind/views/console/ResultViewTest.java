package org.eyo.mastermind.views.console;

import org.eyo.mastermind.models.Result;
import org.eyo.mastermind.views.console.ResultView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResultViewTest {

    private ResultView resultView;

    @BeforeEach
    void setUp(){
        this.resultView = new ResultView(new Result(0, 0, "rrrr"));
    }

    @Test
    void should_write_result(){
        String result = this.resultView.write();

        assertEquals("rrrr --> 0 blacks and 0 whites.", result);
    }

    @Test
    void should_write_new_result_setted(){
        String result = this.resultView.setResult(new Result(1, 1, "bbbb")).write();

        assertEquals("bbbb --> 1 blacks and 1 whites.", result);
    }
}
