package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.models.ProposedCombination;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.utils.Asker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProposalMenuTest {

    private ProposalMenu proposalMenu;
    private ProposalUseCaseController proposalController;
    private Asker testAsker;

    @BeforeEach
    void setUp() {
        this.proposalController = new ProposalUseCaseServerController(new Session());
        this.proposalMenu = new ProposalMenu(this.proposalController);
    }

    @Test
    void should_get_three_commands() {
        assertEquals(3, this.proposalMenu.getCommands().size());
    }

    @Test
    void should_execute_the_menu_first_wrong() {
        this.testAsker = new Asker() {
            int times = 0;

            @Override
            public String ask(String questionMessage) {
                if (times == 0) {
                    times++;
                    return "ghdgfh";
                }
                return "1";

            }
        };
        this.proposalMenu.setAsker(this.testAsker);
        ProposalConsoleViewCommand proposalCommand = (ProposalConsoleViewCommand) this.proposalMenu.getCommands().get(0);
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        proposalCommand.getProposedCombinationView().setAsker(questionMessage -> "rrrr");

        this.proposalMenu.execute();

        assertTrue(this.proposalController.isWinner());
    }

    @Test
    void should_execute_the_menu_first_wrong_null() {
        this.testAsker = new Asker() {
            int times = 0;

            @Override
            public String ask(String questionMessage) {
                if (times == 0) {
                    times++;
                    return null;
                }
                return "1";

            }
        };
        this.proposalMenu.setAsker(this.testAsker);
        ProposalConsoleViewCommand proposalCommand = (ProposalConsoleViewCommand) this.proposalMenu.getCommands().get(0);
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        proposalCommand.getProposedCombinationView().setAsker(questionMessage -> "rrrr");

        this.proposalMenu.execute();

        assertTrue(this.proposalController.isWinner());
    }

    private Asker createAskerFromArray(String[] arrayResponses){
        return new Asker() {
            int times = -1;

            @Override
            public String ask(String questionMessage) {
                times++;
                return arrayResponses[times];
            }
        };
    }

    @Test
    void should_execute_the_menu_put_combination_twice_undo_redo() {
        this.proposalMenu.setAsker(createAskerFromArray(new String[]{"1", "1", "2", "2", "1", "1"}));
        ProposalConsoleViewCommand proposalCommand = (ProposalConsoleViewCommand) this.proposalMenu.getCommands().get(0);
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        proposalCommand.getProposedCombinationView().setAsker(createAskerFromArray(new String[]{"bbbb", "yyyy", "yyyy", "rrrr"}));

        this.proposalMenu.execute();
        this.proposalMenu.execute();
        this.proposalMenu.execute();
        this.proposalMenu.execute();
        this.proposalMenu.execute();
        this.proposalMenu.execute();

        assertTrue(this.proposalController.isWinner());
    }
}
