package org.eyo.mastermind.views.console;

import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.views.console.ProposedCombinationView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProposedCombinationViewTest {

    private ProposedCombinationView proposedCombinationView;

    @BeforeEach
    void setUp(){
        this.proposedCombinationView = new ProposedCombinationView();
    }

    @Test
    void should_read_proposed_combination_from_system(){
        String input = "roro";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Combination proposedCombination = this.proposedCombinationView.readProposedCombination();
        assertEquals("roro", proposedCombination.getsCombination());
    }

    @Test
    void should_read_proposed_combination_from_asker(){
        Asker testAsker = questionMessage -> "rrrr";
        this.proposedCombinationView = new ProposedCombinationView(testAsker);
        Combination proposedCombination = this.proposedCombinationView.readProposedCombination();
        assertEquals("rrrr", proposedCombination.getsCombination());
    }

    @Test
    void should_try_twice_proposed_combination_from_asker(){
        Asker testAsker = new Asker() {
            private int time = 0;
            @Override
            public String ask(String questionMessage) {
                if (time == 0){
                    time++;
                    return "r";
                }
                return "rrrr";
            }
        };
        this.proposedCombinationView = new ProposedCombinationView(testAsker);
        Combination proposedCombination = this.proposedCombinationView.readProposedCombination();
        assertEquals("rrrr", proposedCombination.getsCombination());
    }
}
