package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.controllers.server.ResumeUseCaseServerController;
import org.eyo.mastermind.controllers.server.StartUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.types.StateValue;
import org.eyo.mastermind.views.ProposalInteractView;
import org.eyo.mastermind.views.ResumeInteractView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConsoleViewTest {

    private ConsoleView consoleView;
    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.consoleView = new ConsoleView();
    }

    @Test
    void should_end_game_after_several_tries(){
        setProposal();
        setResume();

        this.consoleView.interact(new StartUseCaseServerController(this.sessionImpl));

        assertEquals(StateValue.IN_GAME, this.sessionImpl.getStateValue());
    }

    @Test
    void should_execute_proposal_controller(){
        setProposal();
        setResume();

        this.consoleView.interact(new ProposalUseCaseServerController(this.sessionImpl));

        assertEquals(StateValue.INITIAL, this.sessionImpl.getStateValue());
    }

    @Test
    void should_execute_resume_controller(){
        setProposal();
        setResume();

        this.consoleView.interact(new ResumeUseCaseServerController(this.sessionImpl));

        assertEquals(StateValue.INITIAL, this.sessionImpl.getStateValue());
    }

    private void setResume() {
        this.consoleView.setResumeView(new ResumeInteractView() {
            private int times = 0;

            @Override
            public boolean interact(ResumeUseCaseController controller) {
                if (times == 0) {
                    times++;
                    return true;
                }
                return false;
            }
        });
    }

    private void setProposal() {
        this.consoleView.setProposalView(new ProposalInteractView() {
            private int times = 0;

            @Override
            public boolean interact(ProposalUseCaseController controller) {
                if (times == 0) {
                    times++;
                    return false;
                }
                return true;
            }
        });
    }
}
