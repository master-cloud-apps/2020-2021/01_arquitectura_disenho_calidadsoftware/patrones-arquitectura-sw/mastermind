package org.eyo.mastermind.models;

import org.eyo.mastermind.types.StateValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class SessionTest {

    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.sessionImpl.reset();
    }

    @Test
    void should_get_state_value(){
        assertEquals(StateValue.INITIAL, this.sessionImpl.getValueState());
    }

    @Test
    void should_move_nex_state(){
        this.sessionImpl.next();
        assertEquals(StateValue.IN_GAME, this.sessionImpl.getValueState());
    }

    @Test
    void should_reset_elements_from_session(){
        this.sessionImpl.next();
        this.sessionImpl.next();
        this.sessionImpl.next();

        this.sessionImpl.reset();

        assertEquals(StateValue.INITIAL, this.sessionImpl.getStateValue());
    }

    @Test
    void should_get_a_proposed_combination_after_adding_one(){
        Combination combination = new ProposedCombination("rrrr");

        this.sessionImpl.addProposedCombination(combination);

        assertNotNull(this.sessionImpl.getProposedCombination(0));
    }

    @Test
    void should_get_the_result_4_blacks_from_a_proposed_combination(){
        Combination combination = new ProposedCombination(this.sessionImpl.getSecretCombination().getsCombination());
        this.sessionImpl.addProposedCombination(combination);

        Result result = this.sessionImpl.getResult(0);
        assertEquals(4, result.getBlacks());
    }

    @ParameterizedTest
    @CsvSource({
            "2, 2, rryy, ryry",
            "2, 0, ppob, pppp",
            "2, 1, rgob, ogyb",
    })
    void should_get_the_result_b_blacks_w_whites_from_h_secret_p_proposed(int blacks, int whites, String secret,
                                                                          String proposed){
        this.sessionImpl.getSecretCombination().setCombination(secret);

        Combination combination = new ProposedCombination(proposed);
        this.sessionImpl.addProposedCombination(combination);

        Result result = this.sessionImpl.getResult(0);
        assertEquals(blacks, result.getBlacks());
        assertEquals(whites, result.getWhites());
        assertEquals(1, this.sessionImpl.getResults().size());
    }

    @Test
    void shoult_have_all_boards_methods(){
        assertFalse(this.sessionImpl.isWinner());
        assertFalse(this.sessionImpl.isLooser());
    }

    @Test
    void should_not_be_undoable(){
        assertFalse(this.sessionImpl.undoable());
        assertFalse(this.sessionImpl.redoable());
    }

    @Test
    void should_be_undoable_after_propose_two_combinations(){
        this.sessionImpl.addProposedCombination(new ProposedCombination("bbbb"));
        this.sessionImpl.addProposedCombination(new ProposedCombination("bbbb"));

        assertTrue(this.sessionImpl.undoable());
    }

    @Test
    void should_undo_second_combination_and_be_not_undoable(){
        this.sessionImpl.addProposedCombination(new ProposedCombination("bbbb"));
        this.sessionImpl.addProposedCombination(new ProposedCombination("rrrr"));

        this.sessionImpl.undo();

        assertFalse(this.sessionImpl.undoable());
        assertTrue(this.sessionImpl.redoable());
        this.sessionImpl.getBoard().getResults();
    }

    @Test
    void should_undo_second_combination_and_redo_and_be_undoable(){
        this.sessionImpl.addProposedCombination(new ProposedCombination("bbbb"));
        this.sessionImpl.addProposedCombination(new ProposedCombination("rrrr"));

        this.sessionImpl.undo();
        this.sessionImpl.redo();

        assertTrue(this.sessionImpl.undoable());
        assertFalse(this.sessionImpl.redoable());
    }
}
