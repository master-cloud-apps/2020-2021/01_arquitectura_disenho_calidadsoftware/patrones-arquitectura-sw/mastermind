package org.eyo.mastermind;

import org.eyo.mastermind.controllers.*;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.models.State;
import org.eyo.mastermind.views.View;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ConsoleMastermindTest {

    private ConsoleMastermind consoleMastermind;
    private View testView;

    @BeforeEach
    void setUp(){
        this.consoleMastermind = new ConsoleMastermind();
        this.testView = new View() {
            @Override
            public void visit(StartUseCaseController startController) {

            }

            @Override
            public void visit(ProposalUseCaseController proposalController) {

            }

            @Override
            public void visit(ResumeUseCaseController resumeController) {

            }

            @Override
            public void interact(AcceptorController useCaseController) {
                State state = ((Session)consoleMastermind.getLogic().getSession()).getState();
                state.next();
            }
        };
        this.consoleMastermind.setView(this.testView);
    }

    @Test
    void should_call_play(){
        assertTrue(this.consoleMastermind.play());
    }


}
